<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Polling Booth</title>
    <link rel="icon" href="images/fav_ico.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.min.css">

    <link rel="stylesheet" type="text/css" href="css/style.css">


    <script src="js/jquery-2.2.3.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>

    <script src="js/script.js"></script>
</head>
<body>
    <div class="lang">
        <span class="lang_trigger"><img src="images/settings.png" alt="" /></span>
        <h3>Change Language</h3>
        <div class="lang_lnks">
            <a href="javascript:void(0)" class="active">മലയാളം</a><a href="index_en.php">English</a>
        </div>
        <div class="lang_foot">
            <button>Close</button>
        </div>
    </div>
    <div class="header">
        <a href="https://play.google.com/store/apps/details?id=com.bodhiinfo.pollingbooth" target="_blank"><img src="images/app_logo_white.png" alt="" /></a>
    </div>
    <div class="arc"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="main_img"><img src="images/main_img.jpg" alt="" /></div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="main_des">

                    <h3>Polling Booth</h3>
                   <p style="font-weight: bold">
                        മൊബൈല്‍ ഫോണ്‍, വോട്ടിങ് മെഷീന്‍, വോട്ടിന്റെ ചരിത്രം
                    </p>
                    <p>
                        ടെക്കികളെന്ന ഓമനപ്പേരില്‍ അറിയപ്പെടുന്ന ഇന്റര്‍നെറ്റ്, മൊബൈല്‍ കുതുകികള്‍ക്ക് എന്ത് രാഷ്ട്രീയം എന്ന് കരുതുന്നവരുണ്ട്. എന്നാല്‍ ഇത്രയും കാലം പലരും മൂടിവെച്ച അറിവെന്ന ഭൂതത്തെ കുടം തുറന്നുവിട്ടവരാണവര്‍. ലോകം ഇപ്പോള്‍ കാതോര്‍ക്കാനാഗ്രഹിക്കുന്ന മഹത്തായ മനുഷ്യമുന്നേറ്റങ്ങളുടെ വിത്തുകള്‍ മുളക്കുന്നത് ഈ ആധുനിക സാങ്കേതിക- വിനിമയ വിദ്യകള്‍ വഴിയാണല്ലോ. മനുഷ്യമുന്നേറ്റത്തിന് മഹത്തായ സംഭാവന നല്‍കിയ ജനാധിപത്യപ്രക്രിയയും അതിന്റെ പ്രത്യക്ഷ പ്രയോഗ രീതിയായ തിരഞ്ഞെടുപ്പും ഈ അറിവിന്റെ പ്രയോഗ വഴിയാണെന്നിരിക്കെ ആധുനിക സാങ്കേതിക വിദ്യയെ തിരഞ്ഞെടുപ്പെന്ന ജനാധിപത്യ മഹോത്സവത്തില്‍ ഉള്‍ച്ചേര്‍ക്കുകയാണിവിടെ. 2016ലെ കേരള നിയമസഭാ തിരഞ്ഞെടുപ്പിന്റെ പോരാട്ടത്തെക്കുറിച്ചും തിരഞ്ഞെടുപ്പ് ചരിത്രത്തെക്കുറിച്ചും വിശദമായ അറിവ് നല്‍കുന്ന, ഒപ്പം നിങ്ങളുടെ മണ്ഡലത്തിലെ സ്ഥാനാര്‍ത്ഥിക്ക് വോട്ട് ചെയ്യാന്‍ അവസരം നല്‍കുക കൂടി ചെയ്യുന്ന മൊബൈല്‍ ആപ്ലിക്കേഷനാണ് ' പോളിംഗ് ബൂത്ത്‌ '.  </p><p>നമ്മുടെ ജനാധിപത്യം നടന്നു തീര്‍ത്ത വഴികളെക്കുറിച്ച്, 2016ലെ പോരാട്ടത്തിലെ പടനായകരെക്കുറിച്ച്... തിരഞ്ഞെടുപ്പും അതിന്റെ ചരിത്രവും ഇനി നിങ്ങളുടെ കൈവെള്ളയില്‍... </p>
	<p style="font-weight: bold">മൊബൈല്‍ ഫോണ് വോട്ടിങ് മെഷീന്‍ ആയി മാറുന്ന ചരിത്ര ഘട്ടം</p>
                    <p>
                        ചായമക്കാനികളിലും ബാര്‍ബര്‍ ഷോപ്പുകളിലും വലിയ രാഷ്ട്രീയം ചര്‍ച്ച ചെയ്ത, ഇപ്പോഴും പൊതുയിടങ്ങളില്‍ രാഷ്ട്രീയം ചര്‍ച്ച ചെയ്യനാഗ്രഹിക്കുന്നവരാണ് മുതിര്‍ന്ന തലമുറ. ആധുനിക സാങ്കേതിക വിദ്യയെ അകലത്തില്‍ നിര്‍ത്തുന്ന ആ തലമുറക്ക് പോലും ഇതുവഴി വരാതിരിക്കാന്‍ കഴിയില്ല. കാരണം ഈ ആപ്ലിക്കേഷന്‍ അവര്‍ക്ക് രാഷ്ട്രീയ ചര്‍ച്ചയുടെ വലിയൊരു കോലായ തുറന്നിടുന്നു.</p>
                    <p style="font-weight: bold">അതെ... തലമുറകള്‍ തമ്മില്‍ സംസാരിക്കുന്നു</p>
                </div>
            </div>
        </div>
        <div class="hrow"></div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="spec_item">
                    <h4 style="background-color:#97cd3b;">വോട്ടിങ് സൗകര്യം</h4>
                    <p> കേരള നിയമസഭാ തിരഞ്ഞെടുപ്പിനെക്കുറിച്ച് പറയുന്ന വോട്ടിങ് സൗകര്യത്തോടെയുള്ള ആദ്യത്തെ മൊബൈല്‍ ആപ്ലിക്കേഷനാണിത്. ഇതില്‍ നിങ്ങളുടെ സ്വന്തം മണ്ഡലത്തിലെ സ്ഥാനാര്ത്ഥിക്ക് വോട്ട് ചെയ്യാന്‍ അവസരമുണ്ട്. പോള്‍ എന്ന ലിങ്കില്‍ നിന്നും മണ്ഡലം തിരഞ്ഞെടുത്ത് ഇഷ്ടമുള്ള സ്ഥാനാര്‍ത്ഥിക്ക് വോട്ടുചെയ്യാം. ഒരു മൊബൈലില്‍ ഒരു വോട്ട് മാത്രമാക്കി പരിമിതപ്പെടുത്തി പോളിങ് ബൂത്തിലെ തിരഞ്ഞെടുപ്പിന്റെ പ്രതീതി യാഥാര്‍ത്ഥ്യം ഉണ്ടാക്കുന്നു.
                    </p>
                     <p>
                        നാടിനേക്കാള്‍ ചൂടോടെ രാഷ്ട്രീയം ചര്‍ച്ച ചെയ്യുന്ന സമൂഹമാണ് പ്രവാസികള്‍, പക്ഷെ പലപ്പോഴും ജനാധിപത്യത്തിന്റെ കളരിക്ക് പുറത്താണവര്‍. പ്രവാസികള്‍ക്ക് തിരഞ്ഞെടുപ്പിന്റെയും വോട്ടിന്റെയും വലിയൊരു ലോകം കയ്യിലെത്തുന്നു.... അതെ പ്രവാസികള്‍ ഇവിടെ വോട്ടു ചെയ്യുന്നു. നാടിന്റെ തിരഞ്ഞെടുപ്പ് മിടിപ്പുകള്‍ തൊട്ടറിയുന്നു. </p>
                </div>
                <div class="spec_item">
                    <h4 style="background-color:#ff5722;">ബൂത്ത് കണ്ടെത്താം</h4>
                    <p>സ്വന്തം മണ്ഡലത്തിലെ പോള്‍ ചെയ്യുന്ന ബൂത്ത് കണ്ടെത്താനുള്ള സൗകര്യമാണിത്</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="mob_banner">
                    <div class="banner_wrap">
                        <ul class="banner">
                            <li><img src="images/mob1.png" alt="" /></li>
                            <li><img src="images/mob2.png" alt="" /></li>
                            <li><img src="images/mob3.png" alt="" /></li>
                            <li><img src="images/mob4.png" alt="" /></li>
                        </ul>
                    </div>
                    <div class="mob_controls">
                        <span class="mob_control" id="mob_prev"></span>
                        <span class="mob_control" id="mob_next"></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="spec_item">
                    <h4 style="background-color:#ff5d5e;">തിരഞ്ഞെടുപ്പ് ചരിത്രം</h4>
                    <p>57 മുതലുള്ള തിരഞ്ഞെടുപ്പ് ചരിത്രം വ്യക്തമാക്കുന്നതാണ് ഈ ഭാഗം. കഴിഞ്ഞ തദ്ദേശ, ലോക്‌സഭാ തിരഞ്ഞെടുപ്പുകളുടെ നിയോജക മണ്ഡലവും, പഞ്ചായത്തും തിരിച്ചുമുള്ള കണക്കുകള്‍. 57 മുതലുള്ള തിരഞ്ഞെടുപ്പുകളില്‍ മണ്ഡലങ്ങളില്‍ ആരൊക്കെ മത്സരിച്ചു. ആര് ജയിച്ചു തുടങ്ങിയതുള്‍ക്കൊള്ളുന്നതാണ് വിവരങ്ങള്‍. കേരളം തിരഞ്ഞെടുപ്പുകളിലൂടെ 2016 വരെ നടന്നടുത്തതെങ്ങിനെയെന്ന് ഇത് വ്യക്തമാക്കുന്നു. </p>
                </div>
                <div class="spec_item">
                    <h4 style="background-color:#f89132;">അഭിപ്രായ വോട്ട്</h4>
                    <p>സംസ്ഥാനത്തെ വികസനവും രാഷ്ട്രീയവുമുള്‍പ്പെടെ നിര്‍ണ്ണായകമായ വിഷയങ്ങളില്‍ നിങ്ങളുടെ അഭിപ്രായം രേഖപ്പെടുത്താനുള്ള സൗകര്യം. വോട്ടിനുമപ്പുറം നിങ്ങള്‍ ഏത് പക്ഷത്ത്, എന്ത്‌കൊണ്ട് നില്‍ക്കുന്നുവെന്നുള്ള രേഖപ്പെടുത്തല്‍.</p>
                </div>
                <div class="spec_item">
                    <h4 style="background-color:#78909c;">ഫലം</h4>
                    <p>ആപ്പില്‍ നടക്കുന്ന വോട്ടെടുപ്പിന്റെ ഇതുവരെയുള്ള റിസള്‍ട്ട് അറിയാനുള്ള സൗകര്യമാണിത്. ഓരോ മണ്ഡലം വേര്‍തിരിച്ച് ഫലം ലഭ്യമാണ്</p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="footer_down">
                        <a href="https://play.google.com/store/apps/details?id=com.bodhiinfo.pollingbooth" target="_blank"><span><img src="images/playstore.png" alt="" /></span>Download Now</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="power">Powered by <a href="http://www.bodhiinfo.com" target="_blank"><img src="images/logo.png" alt="" /></a></div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>