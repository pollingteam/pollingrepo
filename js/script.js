/**
 * Created by shafeequemat on 08-04-2016.
 */
$(function () {
    var slider = $('.banner').bxSlider({
        pager: false,
        auto: true,
        prevSelector: '#mob_prev',
        nextSelector: '#mob_next',
        prevText: '<i class="ion ion-android-arrow-dropleft"></i>',
        nextText: '<i class="ion ion-android-arrow-dropright"></i>',
        onSlideAfter: function() {
            slider.stopAuto();
            slider.startAuto();
        }
    });
    // Language
    $('.lang_trigger').click(function () {
        $('.lang').toggleClass('active');
    });
    $('.lang_foot button').click(function () {
        $('.lang').removeClass('active');
    });
});