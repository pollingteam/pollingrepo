<?php
//database server
define('DB_SERVER', "localhost");
define('DB_USER', "pollingb_user");
define('DB_PASS', "07210721gml");

//database name
define('DB_DATABASE', "pollingb_election");
define('ROWS_PER_PAGE', "15");
//tables
define('TABLE_TBL_NEWS', "tbl_news");
define('TABLE_TBL_RESULT', "tbl_result");
define('TABLE_TBL_SETTINGS', "tbl_settings");

define('TABLE_TBLP_ASSEMBLY', "tblp_assembly");
define('TABLE_TBLP_CANDIDATES', "tblp_candidates");
define('TABLE_TBLP_POLL', "tblp_poll");

define('TABLE_TBLH_PARTY', "tblh_party");
define('TABLE_TBLH_ASSEMBLY_MASTER', "tblh_assembly_master");
define('TABLE_TBLH_LOKSABHA_DETAIL', "tblh_loksabha_detail");
define('TABLE_TBLH_LOKSABHA_VOTE', "tblh_loksabha_vote");
define('TABLE_TBLH_PANCHAYATH_DETAIL', "tblh_panchayath_detail");
define('TABLE_TBLH_MUNNANI', "tblh_munnani");
define('TABLE_TBLH_PANCHAYATH_VOTE', "tblh_panchayath_vote");
define('TABLE_TBLH_PANCHAYATH_TYPE', "tblh_panchayath_type");
define('TABLE_TBLH_ASSEMBLY_DETAIL', "tblh_assembly_detail");
define('TABLE_TBLH_ASSEMBLY_VOTE', "tblh_assembly_vote");
define('TABLE_TBLH_LOKSABHA_MASTER', "tblh_loksabha_master");

define('TABLE_TBLO_OPENION_ANSWERS', "tblo_openion_answers");
define('TABLE_TBLO_OPENION_QUESTION', "tblo_openion_question");
define('TABLE_TBLO_USERS_ANSWER', "tblo_users_answer");
define('TABLE_TBLP_USER', "tblp_user");
define('TABLE_TBL_DISTRICT', "tbl_district");
define('TABLE_TBL_HISTORY_TRACKING', "tbl_history_tracking");

define('TABLE_TBL_FEEDBACK',"tbl_feedback");
define('TABLE_TBL_MESSAGE',"tbl_message");

define('TABLE_TBL_LIVE_RESULT', "tbl_live_result");
?>