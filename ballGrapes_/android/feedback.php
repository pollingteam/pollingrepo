<?php
ini_set('memory_limit','-1');
require("../config/config.inc.php"); 
require("../config/Database.class.php");
require("../config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

$url 	= 	file_get_contents('php://input');
$jsons	= 	urldecode($url);
//$jsons	= 	'{"answerID":"7","openionQustionID":"1","userID":"vishnukrbodhi@gmail.com"}';
$obj	= 	json_decode($jsons);
if($jsons)
{
	$userID 	= 	$App->convert($obj->{'userID'});
	$token 		= 	$App->convert($obj->{'token'});
	$name 		= 	$App->convert($obj->{'name'});
	$phone 		= 	$App->convert($obj->{'phone'});
	$feedback 	= 	$App->convert($obj->{'feedback'});
	
	// for this users opinion
	$tokenExist = $db->existValuesId(TABLE_TBLP_USER," token = '$token'");
	if($tokenExist>0)	
	{
		$data['name']		=	$App->convert($name);
		$data['phone']		=	$App->convert($phone);
		$data['userID']		=	$App->convert($userID);
		$data['feedback']	=	$App->convert($feedback);	
		
		$success	=	$db->query_insert(TABLE_TBL_FEEDBACK,$data)	;
		if($success)
		{
			$row['response'] = 'success';
		}
		else
		{
			$row['response'] = 'failed';
		}
		
	}
	echo json_encode($row);
}
?>