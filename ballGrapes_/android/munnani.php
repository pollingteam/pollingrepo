<?php
ini_set('memory_limit','-1');
require("../config/config.inc.php"); 
require("../config/Database.class.php");
require("../config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

$url 	= 	file_get_contents('php://input');
$jsons	= 	urldecode($url);
$obj	= 	json_decode($jsons);
if($jsons)
{
	$token 		= 	$App->convert($obj->{'token'});
	$tokenExist = 	$db->existValuesId(TABLE_TBLP_USER," token = '$token'");
	if($tokenExist>0)	
	{
		// result of munnani
		$json_munnani = array();

		$munnaniQry = "select count(munnani) as munnaniCount, munnani,ID
							  from ( SELECT munnani ,ID
							  		  from (SELECT COALESCE(SUM(count),0) as totCount, 
							  		               ".TABLE_TBLH_MUNNANI.".ID, ".TABLE_TBLP_CANDIDATES.".candidate, 
							  		               ".TABLE_TBLH_PARTY.".partyName, 
							  		               ".TABLE_TBLP_ASSEMBLY.".assemblyName, 
							  		               ".TABLE_TBLH_MUNNANI.".munnani 
							  		          FROM ".TABLE_TBLP_CANDIDATES." 
							  	   LEFT OUTER JOIN ".TABLE_TBLP_POLL." on ".TABLE_TBLP_CANDIDATES.".ID=".TABLE_TBLP_POLL.".candidatePID 
							  	        INNER JOIN ".TABLE_TBLP_ASSEMBLY." on ".TABLE_TBLP_CANDIDATES.".assemblyPID=".TABLE_TBLP_ASSEMBLY.".ID 
							  	        INNER JOIN ".TABLE_TBLH_MUNNANI." on ".TABLE_TBLP_CANDIDATES.".munnaniID=".TABLE_TBLH_MUNNANI.".ID 
							  	        INNER JOIN ".TABLE_TBLH_PARTY." on ".TABLE_TBLH_PARTY.".ID=".TABLE_TBLP_CANDIDATES.".partyID 
							  	          GROUP BY ".TABLE_TBLP_CANDIDATES.".ID,".TABLE_TBLP_CANDIDATES.".candidate,
							  	          		   ".TABLE_TBLP_ASSEMBLY.".assemblyName,".TABLE_TBLH_MUNNANI.".munnani,
							  	          		   ".TABLE_TBLH_PARTY.".partyName 
							  	          ORDER BY assemblyName asc, totCount desc) tab where totCount>0 GROUP by assemblyName) tab2 GROUP by munnani";
					  // echo $munnaniQry;die;
					   
		$munnaniResult 	= mysql_query($munnaniQry);
		$munnaniArray 	=	array();
		while($munnaniRow = mysql_fetch_array($munnaniResult))
		{
			$rows2['munnaniId']		=	$munnaniRow['ID'];
			$rows2['munnaniName']	=	$munnaniRow['munnani'];
			$rows2['leading']		=	$munnaniRow['munnaniCount'];
			
			array_push($munnaniArray,"'".$munnaniRow['munnani']."'");
			array_push($json_munnani,$rows2);
			
		}
		if(mysql_num_rows($munnaniResult)<9)
		{
			$munnaniArray	=	implode(",",$munnaniArray);
			$notinQry 		= 	"SELECT ID,munnani FROM ".TABLE_TBLH_MUNNANI." WHERE munnani NOT IN($munnaniArray) and munnani !=''";
			$notinResult 	=	mysql_query($notinQry);
			while($notinRow = mysql_fetch_array($notinResult))
			{
				$rows2['munnaniId']		=	$notinRow['ID'];
				$rows2['munnaniName']	=	$notinRow['munnani'];
				$rows2['leading']		=	'0';
				array_push($json_munnani,$rows2);
			}
		}

		$response	=	$json_munnani;
		echo json_encode($response);
	}
}
?>