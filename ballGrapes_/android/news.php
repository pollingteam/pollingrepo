<?php
ini_set('memory_limit','-1');
require("../config/config.inc.php"); 
require("../config/Database.class.php");
require("../config/Application.class.php");


$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

$url 	= 	file_get_contents('php://input');
$jsons	= 	urldecode($url);
$obj	= 	json_decode($jsons);
if($jsons)
{
	$token 		= 	$App->convert($obj->{'token'});
	$tokenExist = 	$db->existValuesId(TABLE_TBLP_USER," token = '$token'");
	if($tokenExist>0)	
	{
		//news
		$json_news 	= 	array();	
		$newsQry 	= 	mysql_query("select ".TABLE_TBL_NEWS.".ID,
											".TABLE_TBL_NEWS.".heading,									
											".TABLE_TBL_NEWS.".description,									
											".TABLE_TBL_NEWS.".newsFrom,									
											".TABLE_TBL_NEWS.".link,									
											".TABLE_TBL_NEWS.".image									
								   	   from ".TABLE_TBL_NEWS."
								   order by ".TABLE_TBL_NEWS.".ID desc							 
									");	
		$newsNum 		= 	mysql_num_rows($newsQry);
								
		if($newsQry>0)
		{		
			
			while($newsRow = mysql_fetch_assoc($newsQry))
			{
				$rows['id'] 			= 	$newsRow['ID'];
				$rows['heading'] 		= 	$newsRow['heading'];
				$rows['description'] 		= 	$newsRow['description'];
				$rows['newsFrom'] 		= 	$newsRow['newsFrom'];
				$rows['link'] 			= 	$newsRow['link'];
				$rows['image'] 			= 	$newsRow['image'];
				//die;
				array_push($json_news,$rows);				
			}
		}
		else
		{
			$rows 			= 	"";		
			array_push($json_news,$rows);
		}

		$response	= 	$json_news;
		echo json_encode($response);
	}
}
?>