<?php
ini_set('memory_limit','-1');
require("../config/config.inc.php"); 
require("../config/Database.class.php");
require("../config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

// question result
$url 	= 	file_get_contents('php://input');
$jsons	= 	urldecode($url);
$obj	= 	json_decode($jsons);
if($jsons)
{
	$qsID 	= 	$App->convert($obj->{'questionID'});
	$token 		= 	$App->convert($obj->{'token'});
	$tokenExist = 	$db->existValuesId(TABLE_TBLP_USER," token = '$token'");
	if($tokenExist>0)	
	{
		$json_answer 	=	array();
		$qry	=	"SELECT ".TABLE_TBLO_USERS_ANSWER.".`answerID`,
							".TABLE_TBLO_OPENION_ANSWERS.".`answer`,
							".TABLE_TBLO_OPENION_ANSWERS.".`answerMal`,
							 count( ".TABLE_TBLO_USERS_ANSWER.".answerID) as ansCount
					   FROM ".TABLE_TBLO_USERS_ANSWER." 
			     inner join ".TABLE_TBLO_OPENION_ANSWERS." on ".TABLE_TBLO_OPENION_ANSWERS.".`ID`=".TABLE_TBLO_USERS_ANSWER.".`answerID` 
			          where ".TABLE_TBLO_USERS_ANSWER.".openionQustionID='$qsID' 
			       group by ".TABLE_TBLO_USERS_ANSWER.".answerID";
		$ansResult	=	mysql_query($qry);
		while($ansRow	=	mysql_fetch_array($ansResult))
		{
			$row['answerID']	=	$ansRow['answerID'];
			$row['answer']		=	$ansRow['answer'];
			$row['answerMal']		=	$ansRow['answerMal'];
			$row['ansCount']	=	$ansRow['ansCount'];
			
			array_push($json_answer,$row);
		}
		
		//settings
		$json_settings 	= 	array();
		$qry	=	mysql_query("SELECT * FROM ".TABLE_TBL_SETTINGS."");
		if(mysql_num_rows($qry)>0)
		{
			while($row	=	mysql_fetch_array($qry))
			{
				
				$row1['ID']			=	$row['ID'];
				$row1['pollStatus']	=	$row['pollStatus'];
				$row1['reason']		=	$row['reason'];
				$row1['googleAd']	=	$row['googleAd'];
				
				array_push($json_settings,$row1);
			}				
		}
		

		$response['Result'] 	=	$json_answer;
		$response['Settings']	= 	$json_settings;
		echo json_encode($response);
		
	}
}
?>