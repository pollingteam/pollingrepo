<?php
ini_set('memory_limit','-1');
require("../config/config.inc.php"); 
require("../config/Database.class.php");
require("../config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

$url 	= 	file_get_contents('php://input');
$jsons	= 	urldecode($url);
//$jsons	= 	'{"answerID":"7","openionQustionID":"1","userID":"vishnukrbodhi@gmail.com"}';
$obj	= 	json_decode($jsons);
if($jsons)
{
	$userID 	= 	$App->convert($obj->{'userID'});
	$token 		= 	$App->convert($obj->{'token'});
	// for this users opinion
	$tokenExist = $db->existValuesId(TABLE_TBLP_USER," token = '$token'");
	if($tokenExist>0)	
	{
		$existId 		=	$db->existValuesId(TABLE_TBLO_USERS_ANSWER," token='$token'");
		if($existId>0)
		{
			$answers = array();
			$answersQry	=	mysql_query("select * from ".TABLE_TBLO_USERS_ANSWER." where token='$token' ");
			while($answerRow = mysql_fetch_array($answersQry))
			{
				$row['userID']				=	$answerRow['userID'];
				$row['openionQustionID']	=	$answerRow['openionQustionID'];
				$row['answerID']			=	$answerRow['answerID'];
				$row['status']				=	'EXIST';
				array_push($answers,$row);
				
			}
		}
		else
		{
			$answers = array();
		}
		
		// all questions
		
		$json_question 	= 	array();	
		$questionQry 	= 	mysql_query("select ".TABLE_TBLO_OPENION_QUESTION.".ID,
												".TABLE_TBLO_OPENION_QUESTION.".question,																				".TABLE_TBLO_OPENION_QUESTION.".questionMal
									   	   from ".TABLE_TBLO_OPENION_QUESTION."
									   order by ".TABLE_TBLO_OPENION_QUESTION.".ID asc							 
										");	
		$questionNum 		= 	mysql_num_rows($questionQry);
		if($questionNum>0)
		{		
			while($questionRow = mysql_fetch_assoc($questionQry))
			{
				$rows['id'] 			= 	$questionRow['ID'];
				$rows['question'] 		= 	$questionRow['question'];
				$rows['questionMal'] 	= 	$questionRow['questionMal'];
				
				array_push($json_question,$rows);			
			}
		}
		
		//answer
		$json_answer 	= 	array();	
		$answerQry 	= 	mysql_query("select ".TABLE_TBLO_OPENION_ANSWERS.".ID,
											".TABLE_TBLO_OPENION_ANSWERS.".openionuestionID,
											".TABLE_TBLO_OPENION_ANSWERS.".answer,	
											".TABLE_TBLO_OPENION_ANSWERS.".answerMal						
								   	   from ".TABLE_TBLO_OPENION_ANSWERS."
								   order by ".TABLE_TBLO_OPENION_ANSWERS.".ID asc							 
									");	
		$answerNum 		= mysql_num_rows($answerQry)	;
		if($answerNum > 0)
		{		
			while($answerRow = mysql_fetch_assoc($answerQry))
			{
				$rows1['id'] 					= 	$answerRow['ID'];
				$rows1['openionuestionID'] 		= 	$answerRow['openionuestionID'];
				$rows1['answer'] 				= 	$answerRow['answer'];
				$rows1['answerMal'] 			= 	$answerRow['answerMal'];

				array_push($json_answer,$rows1);			
			}
		}
		
	    //settings
		$json_settings 	= 	array();
		$qry	=	mysql_query("SELECT * FROM ".TABLE_TBL_SETTINGS."");
		if(mysql_num_rows($qry)>0)
		{
			while($row	=	mysql_fetch_array($qry))
			{
				
				$row1['ID']			=	$row['ID'];
				$row1['pollStatus']	=	$row['pollStatus'];
				$row1['reason']		=	$row['reason'];
				$row1['googleAd']	=	$row['googleAd'];
				
				array_push($json_settings,$row1);
			}				
		}
		
		$response = array();
		$response['Your Opinion']	=	$answers;
		$response['Questions']		=	$json_question;
		$response['Answers']		=	$json_answer;
		$response['Settings']		= 	$json_settings;
		echo json_encode($response);
	}
}

?>