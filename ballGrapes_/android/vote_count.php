<?php
ini_set('memory_limit','-1');
require("../config/config.inc.php"); 
require("../config/Database.class.php");
require("../config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

$url 	= 	file_get_contents('php://input');
$jsons	= 	urldecode($url);
if($jsons)
{
	$obj	=	json_decode($jsons);
	
	$assemblyId	=	$App->convert($obj->{'assemblyPID'});
	$token	=	$App->convert($obj->{'token'});
	$tokenExist = 	$db->existValuesId(TABLE_TBLP_USER," token = '$token'");

	if($tokenExist>0)	
	{
		//$assemblyId	=	'1';
		$totalcount	=	array();
		$countQry 	=	"SELECT COALESCE(SUM(".TABLE_TBLP_POLL.".count),0) as totCount,
								".TABLE_TBLP_CANDIDATES.".ID,
								".TABLE_TBLP_CANDIDATES.".candidate,
								".TABLE_TBLH_PARTY.".partyName,
								".TABLE_TBLH_PARTY.".colorCode,
								".TABLE_TBLP_ASSEMBLY.".assemblyName,
								".TABLE_TBLH_MUNNANI.".munnani
						   FROM ".TABLE_TBLP_CANDIDATES."
				LEFT OUTER JOIN ".TABLE_TBLP_POLL." on ".TABLE_TBLP_CANDIDATES.".ID=".TABLE_TBLP_POLL.".candidatePID
					 INNER JOIN ".TABLE_TBLP_ASSEMBLY." on ".TABLE_TBLP_CANDIDATES.".assemblyPID=".TABLE_TBLP_ASSEMBLY.".ID
					 INNER JOIN ".TABLE_TBLH_MUNNANI." on ".TABLE_TBLP_CANDIDATES.".munnaniID=".TABLE_TBLH_MUNNANI.".ID
					 INNER JOIN ".TABLE_TBLH_PARTY." on ".TABLE_TBLH_PARTY.".ID=".TABLE_TBLP_CANDIDATES.".partyID
						  WHERE ".TABLE_TBLP_CANDIDATES.".assemblyPID='$assemblyId'
					   group by ".TABLE_TBLP_CANDIDATES.".ID,".TABLE_TBLP_CANDIDATES.".candidate,".TABLE_TBLP_ASSEMBLY.".assemblyName,".TABLE_TBLH_MUNNANI.".munnani,".TABLE_TBLH_PARTY.".partyName,".TABLE_TBLH_PARTY.".colorCode";
		$countResult	=	mysql_query($countQry);
		if(mysql_num_rows($countResult)>0)
		{
			while($countRow	=  mysql_fetch_array($countResult))
			{
				$row['mandalam']	=	$countRow['assemblyName'];	
				$row['candidate']	=	$countRow['candidate'];	
				$row['party']		=	$countRow['partyName'];	
				$row['munnani']		=	$countRow['munnani'];;	
				$row['votcount']	=	$countRow['totCount'];
				$row['colorCode']	=	$countRow['colorCode'];
				array_push($totalcount,$row);	
			}
		}
		else
		{
			$row	=	'';
			array_push($totalcount,$row);
		}
		//settings
		$json_settings 	= 	array();
		$qry	=	mysql_query("SELECT * FROM ".TABLE_TBL_SETTINGS."");
		if(mysql_num_rows($qry)>0)
		{
			while($row	=	mysql_fetch_array($qry))
			{
				
				$row1['ID']			=	$row['ID'];
				$row1['pollStatus']	=	$row['pollStatus'];
				$row1['reason']		=	$row['reason'];
				$row1['googleAd']	=	$row['googleAd'];
				
				array_push($json_settings,$row1);
			}				
		}
		$response['ResultResponse']		=	$totalcount;
		$response['Settings']	= 	$json_settings;
		echo json_encode($response);			
	}
}
?>