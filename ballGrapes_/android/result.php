<?php
ini_set('memory_limit','-1');
require("../config/config.inc.php"); 
require("../config/Database.class.php");
require("../config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

$url 	= 	file_get_contents('php://input');
$jsons	= 	urldecode($url);
$obj	= 	json_decode($jsons);
if($jsons)
{
	$token 		= 	$App->convert($obj->{'token'});
	$tokenExist = 	$db->existValuesId(TABLE_TBLP_USER," token = '$token'");
	if($tokenExist>0)	
	{
		//result of candidate
		$json_candidate 	= 	array();
		$candidate	=	"SELECT COALESCE(SUM(".TABLE_TBLP_POLL.".count),0) as totCount,
											".TABLE_TBLP_CANDIDATES.".ID,
											".TABLE_TBLP_CANDIDATES.".candidate,
											".TABLE_TBLH_PARTY.".partyName,
											".TABLE_TBLH_PARTY.".colorCode,
											".TABLE_TBLH_PARTY.".ID as pId,
											".TABLE_TBLP_ASSEMBLY.".assemblyName,
											".TABLE_TBLP_ASSEMBLY.".ID as aId,
											".TABLE_TBLH_MUNNANI.".munnani,
											".TABLE_TBLH_MUNNANI.".ID as mId
									   FROM ".TABLE_TBLP_CANDIDATES."
							LEFT OUTER JOIN ".TABLE_TBLP_POLL." on ".TABLE_TBLP_CANDIDATES.".ID=".TABLE_TBLP_POLL.".candidatePID
								 INNER JOIN ".TABLE_TBLP_ASSEMBLY." on ".TABLE_TBLP_CANDIDATES.".assemblyPID=".TABLE_TBLP_ASSEMBLY.".ID
								 INNER JOIN ".TABLE_TBLH_MUNNANI." on ".TABLE_TBLP_CANDIDATES.".munnaniID=".TABLE_TBLH_MUNNANI.".ID
								 INNER JOIN ".TABLE_TBLH_PARTY." on ".TABLE_TBLH_PARTY.".ID=".TABLE_TBLP_CANDIDATES.".partyID
								   group by ".TABLE_TBLP_CANDIDATES.".ID";
								   	
		$resultQry 	= 	mysql_query($candidate);	
		$resultNum 		= 	mysql_num_rows($resultQry);
		if($resultQry>0)
		{		
			while($resultRow = mysql_fetch_assoc($resultQry))
			{
				$rows['id'] 			= 	$resultRow['ID'];
				$rows['candidateName'] 	= 	$resultRow['candidate'];
				$rows['assemblyPID'] 	= 	$resultRow['aId'];
				$rows['assemblyName'] 	= 	$resultRow['assemblyName'];
				$rows['partyID'] 		= 	$resultRow['pId'];
				$rows['partyName'] 		= 	$resultRow['partyName']." ( ".$resultRow['munnani']." )";
				$rows['munnaniID'] 		= 	$resultRow['mId'];
				$rows['munnaniName'] 	= 	$resultRow['munnani'];		
				$rows['votes'] 		= 	$resultRow['totCount'];
				$rows['colorCode']		= 	$resultRow['colorCode'];		
				array_push($json_candidate,$rows);			
			}
		}
		else
		{
			$rows 			= 	"";		
			array_push($json_candidate,$rows);
		}


		// result of party

		$json_party = array();

$partyQry =  "SELECT  partyID,pName,pcode,mName, munnaniID,COUNT(*) as lead FROM
						 ( SELECT partyID,pName,pcode,mName, munnaniID,assemblyPID FROM
						     (select COALESCE(SUM(".TABLE_TBLP_POLL.".count),0) as pollcount,".TABLE_TBLP_CANDIDATES.".partyID,".TABLE_TBLH_PARTY.".partyName as pName,".TABLE_TBLH_PARTY.".colorCode as pcode ,".TABLE_TBLP_CANDIDATES.".munnaniID,".TABLE_TBLH_MUNNANI.".munnani as mName,".TABLE_TBLH_MUNNANI.".colorCode as mcode,".TABLE_TBLP_CANDIDATES.".priority,".TABLE_TBLP_CANDIDATES.".ID canID,".TABLE_TBLP_CANDIDATES.".assemblyPID FROM ".TABLE_TBLP_CANDIDATES." LEFT JOIN ".TABLE_TBLP_POLL." on ".TABLE_TBLP_CANDIDATES.".ID=".TABLE_TBLP_POLL.".candidatePID INNER JOIN ".TABLE_TBLH_PARTY." on ".TABLE_TBLH_PARTY.".ID=".TABLE_TBLP_CANDIDATES.".partyID INNER JOIN ".TABLE_TBLH_MUNNANI." ON ".TABLE_TBLH_MUNNANI.".ID=".TABLE_TBLP_CANDIDATES.".munnaniID GROUP BY ".TABLE_TBLP_CANDIDATES.".ID ORDER BY assemblyPID, pollcount DESC,priority ASC)  tab1
						  GROUP BY assemblyPID ) tab3
			GROUP BY partyID,pName,pcode,mName, munnaniID ";
		$partyResult = mysql_query($partyQry);
		//echo $partyResult;die;
		while($partyRow = mysql_fetch_array($partyResult))
		{
			$rows1['partyId']		=	$partyRow['partyID'];
			$rows1['partyName']		=	$partyRow['pName'];
			$rows1['munnaniId']		=	$partyRow['munnaniID'];
			$rows1['munnaniName']	=	$partyRow['mName'];
			$rows1['totalVote']		=	$partyRow['lead'];
			$rows1['colorCode']		=	$partyRow['pcode'];
			array_push($json_party,$rows1);

		}

		// result of munnani
		$json_munnani = array();
		
/*	$munnaniQry =	"SELECT mcode,mName, munnaniID,COUNT(*) as lead FROM 
							( SELECT mcode,mName, munnaniID,assemblyPID FROM 
									(select COALESCE(SUM(".TABLE_TBLP_POLL.".count),0) as pollcount,".TABLE_TBLP_CANDIDATES.".partyID,".TABLE_TBLH_PARTY.".partyName as pName,".TABLE_TBLH_PARTY.".colorCode as pcode ,".TABLE_TBLP_CANDIDATES.".munnaniID,".TABLE_TBLH_MUNNANI.".munnani as mName,".TABLE_TBLH_MUNNANI.".colorCode as mcode,".TABLE_TBLP_CANDIDATES.".priority,".TABLE_TBLP_CANDIDATES.".ID canID,".TABLE_TBLP_CANDIDATES.".assemblyPID FROM ".TABLE_TBLP_CANDIDATES." LEFT JOIN ".TABLE_TBLP_POLL." on ".TABLE_TBLP_CANDIDATES.".ID=".TABLE_TBLP_POLL.".candidatePID INNER JOIN ".TABLE_TBLH_PARTY." on ".TABLE_TBLH_PARTY.".ID=".TABLE_TBLP_CANDIDATES.".partyID INNER JOIN ".TABLE_TBLH_MUNNANI." ON ".TABLE_TBLH_MUNNANI.".ID=".TABLE_TBLP_CANDIDATES.".munnaniID GROUP BY ".TABLE_TBLP_CANDIDATES.".ID ORDER BY assemblyPID, pollcount DESC,priority ASC) tab1
							 GROUP BY assemblyPID ) tab3 
					GROUP BY mcode,mName, munnaniID order by munnaniID ";

		$munnaniResult 	= mysql_query($munnaniQry);
		while($munnaniRow = mysql_fetch_array($munnaniResult))
		{
			$rows2['munnaniId']		=	$munnaniRow['munnaniID'];
			$rows2['munnaniName']	=	$munnaniRow['mName'];
			$rows2['totalVote']		=	$munnaniRow['lead'];
			$rows2['colorCode']		=	$munnaniRow['mcode'];
			
			array_push($json_munnani,$rows2);
			
		}
		
*/		
			$rows2['munnaniId']		=	"1";
			$rows2['munnaniName']	=	"UDF";
			$rows2['totalVote']		=	"47";
			$rows2['colorCode']		=	"068ED4";			
			array_push($json_munnani,$rows2);
			
			$rows2['munnaniId']		=	"2";
			$rows2['munnaniName']	=	"LDF";
			$rows2['totalVote']		=	"91";
			$rows2['colorCode']		=	"DE0000";			
			array_push($json_munnani,$rows2);

			$rows2['munnaniId']		=	"3";
			$rows2['munnaniName']	=	"NDA";
			$rows2['totalVote']		=	"1";
			$rows2['colorCode']		=	"F97D09";			
			array_push($json_munnani,$rows2);

			$rows2['munnaniId']		=	"4";
			$rows2['munnaniName']	=	"OTHERS";
			$rows2['totalVote']		=	"1";
			$rows2['colorCode']		=	"AEB7BD";			
			array_push($json_munnani,$rows2);
		




		//settings
		$json_settings 	= 	array();
		$qry	=	mysql_query("SELECT * FROM ".TABLE_TBL_SETTINGS."");
		if(mysql_num_rows($qry)>0)
		{
			while($row	=	mysql_fetch_array($qry))
			{
				
				$row1['ID']			=	$row['ID'];
				$row1['pollStatus']	=	$row['pollStatus'];
				$row1['reason']		=	$row['reason'];
				$row1['googleAd']	=	$row['googleAd'];
				
				array_push($json_settings,$row1);
			}				
		}
		
		// party leading mandalam
		$json_mandalam	=	array();
		$mandalamQry	=	mysql_query("SELECT  partyID,pName,mName, munnaniID,assemblyPID,assemblyName FROM 
			( SELECT partyID,pName,pcode,mName, munnaniID,assemblyPID,assemblyName FROM 
                   (select COALESCE(SUM(".TABLE_TBLP_POLL.".count),0) as pollcount,".TABLE_TBLP_CANDIDATES.".partyID,".TABLE_TBLH_PARTY.".partyName as pName,".TABLE_TBLH_PARTY.".colorCode as pcode ,".TABLE_TBLP_CANDIDATES.".munnaniID,".TABLE_TBLH_MUNNANI.".munnani as mName,".TABLE_TBLH_MUNNANI.".colorCode as mcode,".TABLE_TBLP_CANDIDATES.".priority,".TABLE_TBLP_CANDIDATES.".ID canID,".TABLE_TBLP_CANDIDATES.".assemblyPID,".TABLE_TBLP_ASSEMBLY.".assemblyName FROM ".TABLE_TBLP_CANDIDATES." LEFT JOIN ".TABLE_TBLP_POLL." on ".TABLE_TBLP_CANDIDATES.".ID=".TABLE_TBLP_POLL.".candidatePID INNER JOIN ".TABLE_TBLH_PARTY." on ".TABLE_TBLH_PARTY.".ID=".TABLE_TBLP_CANDIDATES.".partyID INNER JOIN ".TABLE_TBLH_MUNNANI." ON ".TABLE_TBLH_MUNNANI.".ID=".TABLE_TBLP_CANDIDATES.".munnaniID  inner join ".TABLE_TBLP_ASSEMBLY." on ".TABLE_TBLP_CANDIDATES.".assemblyPID=".TABLE_TBLP_ASSEMBLY.".ID GROUP BY ".TABLE_TBLP_CANDIDATES.".ID ORDER BY assemblyPID, pollcount DESC,priority ASC) 
                      tab1 GROUP BY assemblyPID )
tab3 order by partyID");
		
		
		if(mysql_num_rows($mandalamQry)>0)
		{
			while($mandalamRow	=	mysql_fetch_array($mandalamQry))
			{
				$row2['partyID']		=	$mandalamRow['partyID'];
				$row2['partyName']		=	$mandalamRow['pName'];
				$row2['munnaniID']		=	$mandalamRow['munnaniID'];
				$row2['munnaniName']	=	$mandalamRow['mName'];
				$row2['assemblyPID']	=	$mandalamRow['assemblyPID'];
				$row2['assemblyName']	=	$mandalamRow['assemblyName'];
				
				array_push($json_mandalam,$row2);
			}
		}

		// live result
		$json_result	=	array();
		$lresultQry		=	"SELECT ".TABLE_TBL_LIVE_RESULT.".ID,
									".TABLE_TBL_LIVE_RESULT.".assemblyPID,
									".TABLE_TBL_LIVE_RESULT.".candidatePID,
									".TABLE_TBL_LIVE_RESULT.".partyID,
									".TABLE_TBL_LIVE_RESULT.".munnaniID,
									".TABLE_TBL_LIVE_RESULT.".vote,
									".TABLE_TBL_LIVE_RESULT.".status,
									".TABLE_TBLP_ASSEMBLY.".assemblyMalayalam,
									".TABLE_TBLP_CANDIDATES.".candidateMal,
									".TABLE_TBLH_PARTY.".partyName,
									".TABLE_TBLH_MUNNANI.".munnani
							   FROM ".TABLE_TBL_LIVE_RESULT." 
						  LEFT JOIN ".TABLE_TBLP_ASSEMBLY." ON ".TABLE_TBLP_ASSEMBLY.".ID=".TABLE_TBL_LIVE_RESULT.".assemblyPID
						 INNER JOIN ".TABLE_TBLP_CANDIDATES." ON ".TABLE_TBLP_CANDIDATES.".ID=".TABLE_TBL_LIVE_RESULT.".candidatePID
						 INNER JOIN ".TABLE_TBLH_PARTY." ON ".TABLE_TBLH_PARTY.".ID=".TABLE_TBL_LIVE_RESULT.".partyID
						 INNER JOIN ".TABLE_TBLH_MUNNANI." ON ".TABLE_TBLH_MUNNANI.".ID=".TABLE_TBL_LIVE_RESULT.".munnaniID
						   ORDER BY ".TABLE_TBL_LIVE_RESULT.".assemblyPID ASC
							";
		
		$lresultResult = mysql_query($lresultQry);
		while($lresultRow    =	mysql_fetch_array($lresultResult))
		{
			$row3['ID']			=	$lresultRow['ID'];
			$row3['assemblyPID']		=	$lresultRow['assemblyPID'];
			$row3['assemblyName']	=	$lresultRow['assemblyMalayalam'];
			$row3['candidatePID']	=	$lresultRow['candidatePID'];
			$row3['candidateName']	=	$lresultRow['candidateMal'];
			$row3['partyID']		=	$lresultRow['partyID'];
			$row3['partyName']		=	$lresultRow['partyName'];
			$row3['munnaniID']		=	$lresultRow['munnaniID'];
			$row3['munnaniName']		=	$lresultRow['munnani'];
			$row3['vote']			=	$lresultRow['vote'];
			$row3['status']		=	$lresultRow['status'];
			
			array_push($json_result,$row3);
		}
		
		$response	= array();
		$response['Candidate Result']	= 	$json_candidate;
		$response['Party Result']		=	$json_party;
		$response['Munnani Result']		=	$json_munnani;
		$response['Settings']		= 	$json_settings;
		$response['Party Mandalam']		= 	$json_mandalam;
		$response['Live Result']		= 	$json_result;
		echo json_encode($response);
	}
}
		
?>