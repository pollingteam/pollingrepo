<?php
ini_set('memory_limit','-1');
require("../config/config.inc.php"); 
require("../config/Database.class.php");
require("../config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

$url 	= 	file_get_contents('php://input');
$jsons	= 	urldecode($url);
$obj	= 	json_decode($jsons);
if($jsons)
{
	$token 		= 	$App->convert($obj->{'token'});
	$tokenExist = 	$db->existValuesId(TABLE_TBLP_USER," token = '$token'");
	if($tokenExist>0)	
	{
		//question
		$json_question 	= 	array();	
		$questionQry 	= 	mysql_query("select ".TABLE_TBLO_OPENION_QUESTION.".ID,
												".TABLE_TBLO_OPENION_QUESTION.".question									
									   	   from ".TABLE_TBLO_OPENION_QUESTION."
									   order by ".TABLE_TBLO_OPENION_QUESTION.".ID asc							 
										");	
		$questionNum 		= 	mysql_num_rows($questionQry);
		if($questionNum>0)
		{		
			while($questionRow = mysql_fetch_assoc($questionQry))
			{
				$rows['id'] 		= 	$questionRow['ID'];
				$rows['question'] 	= 	$questionRow['question'];
				
				array_push($json_question,$rows);			
			}
		}

		//answer
		$json_answer 	= 	array();	
		$answerQry 	= 	mysql_query("select ".TABLE_TBLO_OPENION_ANSWERS.".ID,
											".TABLE_TBLO_OPENION_ANSWERS.".openionuestionID,								
											".TABLE_TBLO_OPENION_ANSWERS.".answer							
								   	   from ".TABLE_TBLO_OPENION_ANSWERS."
								   order by ".TABLE_TBLO_OPENION_ANSWERS.".ID asc							 
									");	
		$answerNum 		= mysql_num_rows($answerQry)	;
		if($answerNum > 0)
		{		
			while($answerRow = mysql_fetch_assoc($answerQry))
			{
				$rows1['id'] 				= 	$answerRow['ID'];
				$rows1['openionuestionID'] 	= 	$answerRow['openionuestionID'];
				$rows1['answer'] 			= 	$answerRow['answer'];

				array_push($json_answer,$rows1);			
			}
		}
		$response	=	array();

		$response['Opinion Question']	= 	$json_question;
		$response['Opinion Answer']		= 	$json_answer;

		echo json_encode($response);
	}
}
?>