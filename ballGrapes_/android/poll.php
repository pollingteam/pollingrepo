<?php
ini_set('memory_limit','-1');
require("../config/config.inc.php"); 
require("../config/Database.class.php");
require("../config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
//assembly

$url 	= 	file_get_contents('php://input');
$jsons	= 	urldecode($url);
$obj	= 	json_decode($jsons);
 if($jsons)
{
	$token 		= 	$App->convert($obj->{'token'});
	$tokenExist = 	$db->existValuesId(TABLE_TBLP_USER," token = '$token'");
	if($tokenExist>0)	
	{
		$json_assembly 	= 	array();	
		$assemblyQry 	= 	mysql_query("select ".TABLE_TBLP_ASSEMBLY.".ID,
												".TABLE_TBLP_ASSEMBLY.".assemblyName,
												".TABLE_TBLP_ASSEMBLY.".districtID,
												".TABLE_TBL_DISTRICT.".district,
												".TABLE_TBLP_ASSEMBLY.".assemblyMalayalam
									   	   from ".TABLE_TBLP_ASSEMBLY."
									  LEFT JOIN	".TABLE_TBL_DISTRICT." ON ".TABLE_TBL_DISTRICT.".ID=".TABLE_TBLP_ASSEMBLY.".districtID
									   order by ".TABLE_TBLP_ASSEMBLY.".ID asc							 
										");	
		$assemblyNum 		= 	mysql_num_rows($assemblyQry);
		if($assemblyNum>0)
		{		
			while($assenblyRow = mysql_fetch_assoc($assemblyQry))
			{
				$rows['id'] 				= 	$assenblyRow['ID'];
				$rows['assemblyName'] 		= 	$assenblyRow['assemblyName'];
		        $rows['assemblyMalayalam'] 	= 	$assenblyRow['assemblyMalayalam'];
				$rows['districtID'] 		= 	$assenblyRow['districtID'];
				$rows['districtName'] 		= 	$assenblyRow['district'];
				
				array_push($json_assembly,$rows);			
			}
		}
		//candidate
		$json_candidate 	= 	array();	
		$candtQry 	= 	mysql_query("select ".TABLE_TBLP_CANDIDATES.".ID,
											".TABLE_TBLP_CANDIDATES.".assemblyPID,								
											".TABLE_TBLP_CANDIDATES.".candidate,								
											".TABLE_TBLP_CANDIDATES.".candidateMal,								
											".TABLE_TBLP_CANDIDATES.".partyID,								
											".TABLE_TBLP_CANDIDATES.".munnaniID,							
											".TABLE_TBLP_CANDIDATES.".sign								
								   	   from ".TABLE_TBLP_CANDIDATES."
								   order by ".TABLE_TBLP_CANDIDATES.".priority asc							 
									");	
		$candtNum 		= mysql_num_rows($candtQry)	;
		if($candtNum > 0)
		{		
			while($candidateRow = mysql_fetch_assoc($candtQry))
			{
				$rows1['id'] 			= 	$candidateRow['ID'];
				$rows1['assemblyPID'] 	= 	$candidateRow['assemblyPID'];
				$rows1['candidate'] 	= 	$candidateRow['candidate'];
				$rows1['candidateMal'] 	= 	$candidateRow['candidateMal'];
				$rows1['partyID'] 		= 	$candidateRow['partyID'];
				$rows1['munnaniID'] 	= 	$candidateRow['munnaniID'];
		       	$rows1['photo'] 		= 	$candidateRow['ID'].".jpg";
				if ($candidateRow['sign'])
				{
					$rows1['icon'] 		= 	$candidateRow['sign'];
				}
				else
				{
					$rows1['icon'] 		= 	$candidateRow['partyID'].".jpg";
				}
				array_push($json_candidate,$rows1);			
			}
		}

		//district
		$json_district 	= 	array();	
		$districtQry	= 	mysql_query("select ".TABLE_TBL_DISTRICT.".ID,
												".TABLE_TBL_DISTRICT.".district											
									   	   from ".TABLE_TBL_DISTRICT."
									   order by ".TABLE_TBL_DISTRICT.".district ASC
										");	
		$districtNum 	= 	mysql_num_rows($districtQry);
		if($districtNum>0)
		{		
			while($districtRow = mysql_fetch_assoc($districtQry))
			{
				$rows11['id'] 			= 	$districtRow['ID'];
				$rows11['district'] 	= 	$districtRow['district'];


				
				array_push($json_district,$rows11);			
			}
		}
		
		//settings
		$json_settings 	= 	array();
		$qry	=	mysql_query("SELECT * FROM ".TABLE_TBL_SETTINGS."");
		if(mysql_num_rows($qry)>0)
		{
			while($row	=	mysql_fetch_array($qry))
			{
				
				$row1['ID']			=	$row['ID'];
				$row1['pollStatus']	=	$row['pollStatus'];
				$row1['reason']		=	$row['reason'];
				$row1['googleAd']	=	$row['googleAd'];
				
				array_push($json_settings,$row1);
			}				
		}

		$response['Poll Assembly']	= 	$json_assembly;
		$response['Poll Candidate']	= 	$json_candidate;
		$response['District']		= 	$json_district;
		$response['Settings']		= 	$json_settings;
		echo json_encode($response);
	}
}
?>