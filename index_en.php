<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Polling Booth</title>
    <link rel="icon" href="images/fav_ico.png">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.min.css">

    <link rel="stylesheet" type="text/css" href="css/style.css">


    <script src="js/jquery-2.2.3.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>

    <script src="js/script.js"></script>
</head>
<body>
    <div class="lang">
        <span class="lang_trigger"><img src="images/settings.png" alt="" /></span>
        <h3>Change Language</h3>
        <div class="lang_lnks">
            <a href="index.php">മലയാളം</a><a class="active" href="#">English</a>
        </div>
        <div class="lang_foot">
            <button>Close</button>
        </div>
    </div>
    <div class="header">
        <a href="https://play.google.com/store/apps/details?id=com.bodhiinfo.pollingbooth" target="_blank"><img src="images/app_logo_white.png" alt="" /></a>
    </div>
    <div class="arc"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="main_img"><img src="images/main_img.jpg" alt="" /></div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="main_des">

                    <h3>Polling Booth</h3>
                   <p style="font-weight: bold">Mobile phone, voting machine, history of voting</p>
                    <p>
                        Nobody associates the so called ‘Techies’ to politics. We say it's not their area of interest or expertise. But what if they make something so cool that voting and the related topics can reach you by a swipe of your mobile phone. The history of elections for your benefits so that we may have a better idea of whom to vote and whom not to. ‘Polling Booth’ is a mobile application which helps to vote for the candidate in your constituency, as well as knowing the current status and result of 2016 kerala election along with the history and past result.</p><p>The history of kerala democracy and details of the candidates in 2016 election along with many other informations are now in your hand. </p>
	<p style="font-weight: bold">The historical revolution of mobile phones as the voting machine</p>
                    <p>Our elder generation loves to talk politics whenever they get a chance. Such healthy conversations can happen during a tea break or when they are in a barber shop. It can heat up the election proceedings to a new level. Such a wise generation of elders will also get attracted to our mobile app because talks about elections is what we do here. But we don’t just let them talk and get away with their opinion, we let them vote too. Making them more friendly to the technologies.</p>
                    <p style="font-weight: bold">YES...Generations talking together.</p>
                </div>
            </div>
        </div>
        <div class="hrow"></div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="spec_item">
                    <h4 style="background-color:#97cd3b;">Poll</h4>
                    <p>Polling Booth is the first mobile application which has information about Kerala Legislative Assembly election along with the provision to cast vote. Poll facility helps you to mark vote to your candidate in few clicks by selecting your constituency. The application restricts polling to one vote from one mobile, to keep the genuinity of the vote and bring the original feel of an election.
                    </p>
                     <p>
                        NRI’s are the one who discuss more about election than the people living inside the country, but the truth is they cannot cast their vote or express their views to become the part of democracy. By this application that gap will get filled and NRI’s will get an opportunity to express their views in their constituency.</p>
                </div>
                <div class="spec_item">
                    <h4 style="background-color:#ff5722;">Find my booth</h4>
                    <p>It is a facility to find your booth to cast vote  in your constituency</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="mob_banner">
                    <div class="banner_wrap">
                        <ul class="banner">
                            <li><img src="images/mob1.png" alt="" /></li>
                            <li><img src="images/mob2.png" alt="" /></li>
                            <li><img src="images/mob3.png" alt="" /></li>
                            <li><img src="images/mob4.png" alt="" /></li>
                        </ul>
                    </div>
                    <div class="mob_controls">
                        <span class="mob_control" id="mob_prev"></span>
                        <span class="mob_control" id="mob_next"></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="spec_item">
                    <h4 style="background-color:#ff5d5e;">Election History</h4>
                    <p>This section includes history of election from the year 1957. Panchayat wise and constituency wise statistics of Lok Sabha and Legislative Assembly elections, shows how Kerala walked through elections to reach here in 2016.</p>
                </div>
                <div class="spec_item">
                    <h4 style="background-color:#f89132;">Opinion Poll</h4>
                    <p>It helps you to express your views on critical subjects like politics and state development. Beyond just casting your vote, you can express your support for particular political front and for what reason you are supporting them.</p>
                </div>
                <div class="spec_item">
                    <h4 style="background-color:#78909c;">Result</h4>
                    <p>Facility to know the status and result of voting which has been carried through ‘Polling Booth’ mobile application. Constituency wise results are available in this section. </p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="footer_down">
                        <a href="https://play.google.com/store/apps/details?id=com.bodhiinfo.pollingbooth" target="_blank"><span><img src="images/playstore.png" alt="" /></span>Download Now</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="power">Powered by <a href="http://www.bodhiinfo.com" target="_blank"><img src="images/logo.png" alt="" /></a></div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>